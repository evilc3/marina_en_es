#!/usr/bin/env bash
export PYTHONPATH="../":"${PYTHONPATH}"
# export WANDB_PROJECT=dmar
export MAX_LEN=128
export BS=32
python distillation.py \
  --learning_rate=3e-4 \
  --do_train \
  --gpus 0 \
  --output_dir marina_student/ \
  --data_dir /content/marina_en_es/kde4_dataset \
  --val_check_interval 0.25 \
  --teacher Helsinki-NLP/opus-mt-en-es \
  --max_source_length $MAX_LEN --max_target_length $MAX_LEN --val_max_target_length $MAX_LEN --test_max_target_length $MAX_LEN \
  --student_decoder_layers 1 --student_encoder_layers 6 \
  --freeze_encoder --freeze_embeds \
  --model_name_or_path IGNORED \
  --alpha_hid=3. \
  --train_batch_size=$BS --eval_batch_size=$BS \
  --tokenizer_name Helsinki-NLP/opus-mt-en-es \
  --warmup_steps 500 --logger_name default \
  --task translation --normalize_hidden --num_sanity_val_steps=0 \
  "$@"